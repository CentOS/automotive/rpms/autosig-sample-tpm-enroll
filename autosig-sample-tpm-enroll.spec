Name:           autosig-sample-tpm-enroll
Version:        0.2
Release:        1%{?dist}
Summary:        Auto-enroll a TPM key at startup for dm-crypt

License:        MIT
Source0:        autosig-sample-tpm-enroll
Source1:        autosig-sample-tpm-enroll.service

Requires: cryptsetup
Requires: clevis-luks
Requires: jq

%description
Auto-enroll a TPM key at startup for dm-crypt

%prep

%build

%install

mkdir -p %{buildroot}%{_bindir}
install  -m 0755 %{SOURCE0} %{buildroot}%{_bindir}/

mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/

%post
systemctl --no-reload enable autosig-sample-tpm-enroll.service

%files
%{_bindir}/autosig-sample-tpm-enroll
%{_unitdir}/autosig-sample-tpm-enroll.service

%changelog
* Wed May  4 2022 Alexander Larsson <alexl@redhat.com> - 0.2-1
- Fixed executable path
- Don't enroll if no TPM devices

* Wed Apr 20 2022 Alexander Larsson <alexl@redhat.com>
- Initial version
