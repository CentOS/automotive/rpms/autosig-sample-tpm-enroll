# autosig-sample-tpm-enroll


This is a simple service that enrolls a TPM key for the rootfs luks
partition on the first boot, and removes the "initial" passphrase.
